#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <curand_kernel.h>

// Parallel reduction (Only works for powers of 2!)
// Not my best work but I am on a tight timeline
__global__ void reductionKernel(unsigned int N, float *in) {

  // Get the global thread
  int id = threadIdx.x + blockIdx.x * blockDim.x;
  // The variable we will use for the reduction
  int n = N / 2;

  // Process if in range
  if (id < N) {

    // int i = 2;
    while (n >= 1) {
      // if (id == 0) {
      //   printf("n = %d\n", n);
      // }
      if (id < n) {
        *(in + id) += *(in + id + n);
      }
      n /= 2;
    }

    // Print the final result
    if (id == 0) {
      printf("Result = %f\n", *in / N);
    }

  }

}

// Sets up the RNG
__global__ void setup_kernel(curandState *state) {

    int id = threadIdx.x + blockIdx.x * blockDim.x;
    /* Each thread gets same seed, a different sequence
       number, no offset */
    curand_init(1234, id, 0, &state[id]);
}

// Computes the algorithm for the problem
__global__ void triangleKernel(unsigned int N, float *results, curandState *state) {

  // Get the global thread
  int id = threadIdx.x + blockIdx.x * blockDim.x;

  // Process if in range
  if (id < N) {

    // Each thread will do n iterations
    unsigned int n = 999999;

    // The variables which will hold the random values
    float x;
    float y;
    // The values above, but they will be ordered (a < b)
    float a;
    float b;
    // The lengths of the sides of the triangle
    float length1;
    float length2;
    float length3;
    // The number of 'successes' we have
    unsigned int hits = 0;

    // NEW RNG HERE
    // Copy state to local memory for efficiency
    curandState localState = *(state + id);

    for (unsigned int i = 0; i < n; i++) {
      // Generate the random numbers
      x = curand_uniform(&localState);
      y = curand_uniform(&localState);

      // Sort them
      if (x < y) {
        a = x;
        b = y;
      } else {
        b = x;
        a = y;
      }
      // Figure out the lengths
      length1 = a;
      length2 = b - a;
      length3 = (float)1 - b; // Make sure this is right
      // Determine which length is the largest
      if (length1 > length2 && length1 > length3) {
        // Length 1 is the biggest one
        // Check if 2 + 3 are at least as big as 1
        if (length2 + length3 > length1) {
          hits++;
        }
      }
      else if (length2 > length1 && length2 > length3) {
        // Length 2 is the biggest one
        // Check if 1 + 3 are at least as big as 2
        if (length1 + length3 > length2) {
          hits++;
        }
      }
      else {
        // Length 3 is the biggest one
        // Check if 1 + 2 are at least as big as 3
        if (length1 + length2 > length3) {
          hits++;
        }
      }
      
    }
    // printf("thread %d got %d hits\n", id, hits);
    // The number of 'hits' (AKA 'successes')
    *(results + id) = (float)hits / n;

    // Copy state back to global memory
    *(state + id) = localState;
  }

}

// Launches the kernels
// N: The number of threads to launch
void launcher(unsigned int N) {

  // Number of threads per thread-block
  unsigned int t = 256;
  // Number of thread-groups
  unsigned int g = (N + t - 1) / t;
  // Total threads
  unsigned int totalThreads = t * g;
  printf("Total number of threads: %d\n", t * g);
  printf("Number of thread-groups: %d\n", g);
  printf("Ideal performance currently at 2048 threads\n");
  // The state of the RNG
  curandState *devStates;
  cudaMalloc((void **)&devStates, totalThreads * sizeof(curandState));

  // Set up the RNG
  setup_kernel<<< g, t >>>(devStates);

  // Allocate memory
  // Vector of outputs
  float *d_results;
  cudaMalloc(&d_results, N * sizeof(float));

  // Call kernel (we are not transferring any data there)
  triangleKernel <<< g, t >>>(N, d_results, devStates);

  // Call reduction kernel
  reductionKernel <<< g, t >>>(N, d_results);

  // Free memory
  cudaFree(d_results);
  d_results = NULL;

}

int main(int argc, char **argv) {

  // Check parameters
  // Terminate program if user did not provide number of threads to use
  if (argc != 2) {
    printf("Usage: ./triangleMC <number of threads (should be a power of 2)>\n");
    exit(1);
  }

  launcher(atoi(*(argv + 1)));

  return 0;
}

